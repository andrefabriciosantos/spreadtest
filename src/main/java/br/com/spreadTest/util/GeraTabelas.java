package br.com.spreadTest.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GeraTabelas {

	  public static void main(String[] args) {
	    EntityManagerFactory factory = Persistence.
	          createEntityManagerFactory("speadPersistenceUnit");

	    factory.close();
	  }
	}