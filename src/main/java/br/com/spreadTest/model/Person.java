package br.com.spreadTest.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Person {

	@Id
	@GeneratedValue
	public Long id;
	
	public String firstName;
	public String lastName;
	public String linkdnUrl;
	public String whatsapp;
	public String mail;

	// for joing the tables (many-to-many)
	@XmlTransient
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "person_skill", joinColumns = { @JoinColumn(name = "personId") }, inverseJoinColumns = {
			@JoinColumn(name = "skillId") })
	private Set<Skill> skills;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLinkdnUrl() {
		return linkdnUrl;
	}

	public void setLinkdnUrl(String linkdnUrl) {
		this.linkdnUrl = linkdnUrl;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	
	public Set<Skill> getSkills() {
		return skills;
	}

	public void setSkills(Set<Skill> skills) {
		this.skills = skills;
	}

	
	
}


