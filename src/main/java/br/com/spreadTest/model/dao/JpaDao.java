package br.com.spreadTest.model.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

public abstract class JpaDao<T> implements Dao<T> {

	@PersistenceContext(name="spreadPersistenceUnit")
	protected EntityManager entityManager;
	
	Class<T> entityClass;

	public JpaDao() {
		super();

	
		ParameterizedType genericSuperClass = (ParameterizedType)getClass().getGenericSuperclass();
		this.entityClass = (Class)genericSuperClass.getActualTypeArguments()[0];
		

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("spreadPersistenceUnit");
		entityManager = factory.createEntityManager();

	}
	
	

	public EntityManager getEntityManager() {
		return entityManager;
	}



	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}



	@Override
	public void persist(T entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
		

	}

	@Override
	public void remove(T entity) {
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.getTransaction().commit();

	}

	@Override
	public T finById(Long id) {
		return entityManager.find(entityClass, id);
	}

}
