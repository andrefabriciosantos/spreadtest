package br.com.spreadTest.model.dao;

import java.util.List;
import java.util.Set;

import br.com.spreadTest.model.Skill;

public interface SkillDao extends Dao<Skill>{

	public List<Skill> findSkillByPerson(Long personId);

}
