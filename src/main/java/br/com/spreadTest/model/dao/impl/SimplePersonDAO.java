package br.com.spreadTest.model.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import br.com.spreadTest.model.Person;
import br.com.spreadTest.model.dao.JpaDao;
import br.com.spreadTest.model.dao.PersonDao;

@Component
public class SimplePersonDAO extends JpaDao<Person> implements PersonDao {

	@Override
	public List<Person> findPersonNoSkill(Long skillId) {

		TypedQuery<Person> query = entityManager
				.createQuery("SELECT p FROM Person p WHERE p.id not in "
						+ "(SELECT distinct p2.id FROM Person p2 INNER JOIN p2.skills s WHERE s.id in :skillId) ", Person.class);

		query.setParameter("skillId", skillId);
		
		List<Person> results = query.getResultList();

		return results;
	}

	@Override
	public List<Person> findPersonBySkill(Long skillId) {

		TypedQuery<Person> query = entityManager
				.createQuery("SELECT p FROM Person p INNER JOIN p.skills s " + "WHERE s.id in :skillId  ", Person.class);
		


		query.setParameter("skillId", skillId);
		
		List<Person> results = query.getResultList();

		return results;
	}

}
