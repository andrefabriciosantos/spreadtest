package br.com.spreadTest.model.dao.impl;

import java.util.List;
import java.util.Set;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import br.com.spreadTest.model.Person;
import br.com.spreadTest.model.Skill;
import br.com.spreadTest.model.dao.JpaDao;
import br.com.spreadTest.model.dao.SkillDao;

@Component
public class SimpleSkillDAO extends JpaDao<Skill> implements SkillDao {

	@Override
	public List<Skill> findSkillByPerson(Long personid) {

		TypedQuery<Skill> query = entityManager
				.createQuery("SELECT s FROM Skill s INNER JOIN s.persons p " + "WHERE p.id in :personid  ", Skill.class);
		


		query.setParameter("personid", personid);
		
		List<Skill> results = query.getResultList();

		return results;
	}

}
