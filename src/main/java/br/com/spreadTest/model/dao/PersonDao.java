package br.com.spreadTest.model.dao;

import java.util.List;
import java.util.Set;

import br.com.spreadTest.model.Person;

public interface PersonDao extends Dao<Person>{

	public List<Person> findPersonNoSkill(Long skillId);

	public List<Person> findPersonBySkill(Long skillId);

}
