package br.com.spreadTest.model.dao;

public interface Dao<T> {

	void persist(T entity);
	void remove(T entity);
	T finById(Long id);
	
}
