package br.com.spreadTest.resource;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.spreadTest.model.Person;
import br.com.spreadTest.model.dao.PersonDao;

@Component
@Path("/person")
public class PersonResource {
	
    private PersonDao dao;
	
    @Autowired
	public PersonResource(PersonDao dao) {
		this.dao = dao;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Person getPersonById(@PathParam("id") Long id) {

		Person person = dao.finById(id);
		return person;
	}

	
	//b) Get all persons including relationships (skill)
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Person> getPersonBySkill(@QueryParam("skill") Long skill,
										@QueryParam("noskill") Long noskill) {

		
		
		if (skill!= null)
			return dao.findPersonBySkill(skill);
		
		if (noskill!= null)
			return dao.findPersonNoSkill(noskill);
		
		
		return null;
	}
	


	
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public Person createPerson(Person person) {
		dao.persist(person);
		return person;
	}

	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Person updatePerson(@PathParam("id") Long id, Person person) {
		Person personEntity = dao.finById(id);
		
		personEntity.setFirstName(person.getFirstName());
		personEntity.setLastName(person.getLastName());
		personEntity.setLinkdnUrl(person.getLinkdnUrl());
		personEntity.setMail(person.getMail());
		personEntity.setWhatsapp(person.getWhatsapp());
		
		
		dao.persist(personEntity);
		return personEntity;
	}

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Person removePerson(@PathParam("id") Long id) {

		Person personEntity = dao.finById(id);
		
		dao.remove(personEntity);;
		return personEntity;
	}
	
	
	

	
	
	
}