package br.com.spreadTest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.spreadTest.model.Skill;
import br.com.spreadTest.model.dao.SkillDao;

@Component
@Path("/skill")
public class SkillResource {

	
    private SkillDao dao;
	
    @Autowired
	public SkillResource(SkillDao dao) {
		this.dao = dao;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Skill getSkillById(@PathParam("id") Long id) {

		Skill skill = dao.finById(id);
		return skill;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Skill> getSkillByPerson(@QueryParam("person") Long person) {

		List<Skill> skills = dao.findSkillByPerson(person);
		
		
		return skills;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public Skill createSkill(Skill skill) {
		dao.persist(skill);
		return skill;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Skill updateSkill(@PathParam("id") Long id, Skill skill) {
		Skill skillEntity = dao.finById(id);
		
		skillEntity.setTag(skill.getTag());
		skillEntity.setDescription(skill.getDescription());
		
		dao.persist(skillEntity);
		return skillEntity;
	}

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Skill removeSkill(@PathParam("id") Long id) {
		//SkillDao skillDAO = new SimpleSkillDAO();
		Skill skillEntity = dao.finById(id);
		
		dao.remove(skillEntity);;
		return skillEntity;
	}
	
}
